import React, { useState } from "react";

const Player = ({ name, symbol }) => {
  const [isEditing, setIsEditing] = useState(false)
  const [playerName, setPlayerName] = useState(name)

  const handleEdit = () => {
    setIsEditing((editing) => !editing)
    
  }

  function handleChange(event){
    setPlayerName(event.target.value)
  }

  let editablePlayerName = <span className="player-name">{playerName}</span>
  if (isEditing) {
    editablePlayerName = <input type='text' onChange={handleChange} value={playerName} required />
  }
  return (
    <li>
      <span className="player">
        {editablePlayerName}
        <span className="player-symbol">{symbol}</span>
      </span>
      <button onClick={handleEdit}>
        {isEditing ? 'Save' : 'Edit'}
      </button>

    </li>
  );
};

export default Player;
